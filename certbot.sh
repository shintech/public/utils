#!/usr/bin/env bash

set -e
set -u
set -o pipefail

rsa_key_size=4096
usage="script usage: $(basename $0) [-h] [-s] [-d domain] [-e email]"
staging=""

helptext=$(cat <<-END
Help:
  -d -> The domain secure with SSL certificate *required
  -e -> The email to use for Let's Encrypt *required
  -s -> Run Let's Encrypt in a staging environment
  -h -> Show the help menu
END
)

while getopts 'shd:e:' OPTION; do
  case "$OPTION" in
    h)
      echo "${usage}" >&2
      echo "${helptext}" >&2
      exit 1
      ;;
    d)
      domains=("${domains[@]}" "${OPTARG}" )
      ;;
    s)
      staging="--staging"
      ;;
    e)
      email=$OPTARG
      ;;
    ?)
      echo "${usage}" >&2
      exit 1
      ;;
  esac
done

if [ -z "${domains+x}" ]; then
  printf "Please provide at least one domain...\n$usage" >&2;
  exit 1;
fi

if [ -z "${email+x}" ]; then
  printf  "Please provide an email address:\n$usage" >&2
  exit 1
fi

shift "$(($OPTIND -1))"

path=/etc/letsencrypt/live/$domains

if [ -d "$path" ]; then
  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
else
  mkdir $path
fi

echo Email: $email
echo Domains to secure: "${domains[*]}"
echo Certificate path: $path

if [ ! -e "/etc/nginx/options-ssl-nginx.conf" ] || [ ! -e "/etc/ssl/dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/tls_configs/options-ssl-nginx.conf > "/etc/nginx/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem > "/etc/ssl/ssl-dhparams.pem"
  echo
fi

openssl req -x509 -nodes -newkey rsa:1024 -days 1 -keyout "$path/privkey.pem"  -out "$path/fullchain.pem" --subj '/CN=localhost'

nginx -s reload

rm -Rf /etc/letsencrypt/live/$domains && \
rm -Rf /etc/letsencrypt/archive/$domains && \
rm -Rf /etc/letsencrypt/renewal/$domains.conf

domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

certbot certonly --nginx --email $email --rsa-key-size $rsa_key_size --agree-tos --force-renewal $domain_args $staging

nginx -s reload
